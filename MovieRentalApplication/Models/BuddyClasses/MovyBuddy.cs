﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieRentalApplication.Models
{
    [MetadataType(typeof(MovieBuddy))]
   
    public partial class Movie
    { }


    sealed class MovieBuddy
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }

        [Display(Name = "Release Date")]
        public string ReleaseDate { get; set; }

    }
}